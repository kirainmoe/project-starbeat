export const strings = {
    "zh-CN": {
        config: "配置文件",
        keyboardLight: "键盘灯",
        tools: "工具",
        lab: "实验室",

        keyboardStyle: "键盘灯设置",
        cannotRunningOnWindows: "不兼容的平台",
        cannotRunningOnWinDescription:
            "由于 Microsoft Windows 系统对 HID 设备的限制，Tongfang Hackintosh Utility 的键盘灯控制功能无法在 Windows 平台上使用。",
        useOfficialControlCenter: "请使用官方软件调节键盘灯；你仍然可以使用 Tongfang Hackintosh Utility 的其它功能。",
        hidCommFailed: "无法与 HID 设备通信",
        hidCommFailedReason:
            "Tongfang Hackintosh Utility 在与 HID 设备通信的时候遇到了错误，导致无法加载必要组件。造成此问题的原因可能是：",
        hidCommFailedNotFound:
            "在此电脑上没有找到兼容设备。请确认你的系统能识别到：制造商 ID 为 0x048d, 产品 ID 为 0xce00 的 ITE Devices(8291)。",
        hidCommFailedRevisionNotMatch:
            "在此电脑上找到了兼容设备，但 ITE 版本 (revision) 不为 0.02。",
        hidCommFailedLinuxUnauthorized:
            "Tongfang Hackintosh Utility 正在 Linux 下运行，但未能获取 root 权限，无权访问 HID 设备。",
        monoColor: "固定颜色",
        breathing: "呼吸",
        wave: "波浪",
        rainbow: "彩虹",
        flash: "闪烁",
        mix: "渐变",
        brightness: "键盘灯亮度",
        speed: "变换速度",

        toolsDescription: "部分功能需要授权。",
        fixSleep: "调节睡眠参数",
        fixSleepDescription: "调节睡眠参数，修复睡眠睡死、自动唤醒等问题。",
        hiDPI: "开启 HiDPI",
        hiDPIDescription:
            "在笔记本内建屏幕中启用缩放 (HiDPI) 功能，降低可视分辨率使得文字渲染更清晰(需要重启)。",
        kextCache: "重建 Kext 缓存",
        kextCacheDescription: "重建内核拓展缓存，以使某些对 Kext 的更改生效。",
        installTongfangFnDaemon: "安装快捷键守护程序",
        installTongfangFnDaemonDescription:
            "安装同方 Fn 快捷键守护程序，以便使 Fn+F1~F7 的功能正常使用。",
        pleaseWait: "操作正在执行中，这可能需要几十秒或更长时间，请等待操作完成...",
        success: "成功",
        state: "状态",
        installed: "已安装",
        notInstalled: "未安装",
        successDescription: "所执行的操作已完成。",
        ToolboxCannotRunningOnWinDescription:
            "工具箱中的功能仅用于优化在 macOS 系统下的使用体验，无法在 Windows / Linux 平台上使用。",

        configure: "配置文件",
        configureDescription: "管理、更新和定制 OpenCore 配置文件。",
        laptopModel: "笔记本机型",
        selectModel: "选择机型或模具型号",
        injectOption: "驱动/补丁注入选项（如无需要或无硬件请不要勾选）",
        injectAirport: "添加博通无线网卡驱动",
        injectIntelBluetooth: "添加因特尔蓝牙驱动",
        injectBrcmBluetooth: "添加博通蓝牙驱动",
        injectHoRNDIS: "添加 USB 网络共享驱动",
        inject4KSupport: "添加 4K 内屏补丁",
        disablePM981: "添加 PM981 屏蔽补丁",
        smbiosInfo: "硬件识别信息",
        getSMBIOSFromGeneration: "已随机生成",
        getSMBIOSFromSystem: "已从系统读取",
        smbiosModel: "型号",
        smbiosSN: "序列号",
        smbiosMLB: "主板序列号",
        smbiosSmUUID: "系统 UUID",
        versionInfo: "版本信息",
        localVersion: "当前引导版本",
        latestVersion: "最新开发版",
        getLatest: "获取最新配置文件",
        downloadWait: "正在下载，请等待",
        successInfo:
            "已将最新 OpenCore 配置文件下载到当前用户桌面的 `Tongfang_EFI` 文件夹中。请将桌面的 Tongfang_EFI/BOOT 和 Tongfang_EFI/OC 复制到 U 盘或硬盘中。",
        successInstructionUSB:
            "对于 U 盘引导用户：请将 BOOT 和 OC 文件夹复制到 U 盘 ESP 分区的 EFI/BOOT 和 EFI/OC 下。如果 ESP 分区中已存在 OC 文件夹，请先将其删除之后再粘贴。",
        successInstructionHD:
            "对于硬盘引导用户：请将 BOOT 内的 BOOTx64.efi 改名为 OpenCore.efi，然后将 BOOT 和 OC 复制到硬盘 ESP 分区的 EFI/BOOT 和 EFI/OC 下，添加 UEFI 启动项指向 EFI/BOOT/OpenCore.efi （而不是指向 EFI/OC/OpenCore.efi !）。如果 ESP 分区中已存在 OC 文件夹，请先将其删除之后再替换。",
        backward: "我知道了，这就去和苹果对线",
        requirement4k:
            "请注意：同方 GJ5CN64 / GI5CN54 模具，需要解锁 BIOS 或使用 UEFI Shell 修改 DVMT Pre-allocated 大小为 64M 后方可支持 4K 内屏，否则会导致内核崩溃.",

        youAreUsing: "你正在使用",
        officialLatest: "官方最新版本是",
        updateRemind:
            "为了防止发生兼容性问题，请前往 https://starbeat.kirainmoe.com 更新 Tongfang Hackintosh Utility 后再管理配置文件。",
        downloadSource: "更新源",
        recommend: '推荐'
    },
    en: {
        config: "Configuration",
        keyboardLight: "Keyboard",
        tools: "Tools",
        lab: "Laboratory",

        keyboardStyle: "Keyboard Light Style",
        cannotRunningOnWindows: "Incompatible Platform",
        cannotRunningOnWinDescription:
            "Due to the limitation of Microsoft Windows on accessing HID devices，the keyboard light controlling function cannot work on Windows.",
        useOfficialControlCenter:
            "Please use the official Control Center to set the keyboard light. You can still use other functions of Tongfang Hackintosh Utility.",
        hidCommFailed: "Unable to communicate with HID device",
        hidCommFailedReason:
            "Error occurred when Tongfang Hackintosh Utility was communicating with HID device, which caused the failure of loading the component. The reason may be:",
        hidCommFailedNotFound:
            'No compatible device was found on this computer. Please ensure that your system recognizes "ITE Devices (8291)" (vendorID 0x048d, productID 0xce00).',
        hidCommFailedRevisionNotMatch:
            "Compatible device was found on this computer, but its revision isn't 0.02.",
        hidCommFailedLinuxUnauthorized:
            "You are running this program on Linux without without root permission.",
        monoColor: "Fixed Color",
        breathing: "Breathing",
        wave: "Wave",
        rainbow: "Rainbow",
        flash: "Flash",
        mix: "Mix",
        brightness: "Brightness",
        speed: "Speed",

        toolsDescription: "Some operation requires to be done as superuser.",
        fixSleep: "Fix sleep",
        fixSleepDescription: 'Run "pmset" command to fix broken sleep.',
        hiDPI: "Enable HiDPI",
        hiDPIDescription: "Make the font more clear.",
        kextCache: "Rebuild kextcache",
        kextCacheDescription: 'Run "kextcache -i /" command to refresh the kernel extension cache.',
        installTongfangFnDaemon: "Install Fn Daemon",
        installTongfangFnDaemonDescription:
            "Install Tongfang Fn-shortcut Daemon to fix function keys.",
        pleaseWait: "Please wait, it will take several seconds to perform the operation...",
        success: "Success",
        state: "Status ",
        installed: "Installed",
        notInstalled: "Uninstalled",
        successDescription: "Done.",
        ToolboxCannotRunningOnWinDescription: "Toolbox can only be used on macOS.",

        configure: "Configuration",
        configureDescription: "Manage, update and customize OpenCore config.",
        laptopModel: "Laptop Model",
        selectModel: "Select a model or barebone...",
        injectOption: "Kext/Patch Injection（Do not check the options that you don't need）",
        injectAirport: "Broadcom Airport Fix",
        injectIntelBluetooth: "Intel Bluetooth",
        injectBrcmBluetooth: "Broadcom Bluetooth",
        injectHoRNDIS: "USB Network Tethering",
        inject4KSupport: "4K Resolution Screen",
        disablePM981: "Disable Incompatible NVMe",
        smbiosInfo: "SMBIOS",
        getSMBIOSFromGeneration: "Randomly generated",
        getSMBIOSFromSystem: "Read from system",
        smbiosModel: "Model",
        smbiosSN: "SerialNumber",
        smbiosMLB: "Motherboard SerialNumber",
        smbiosSmUUID: "System UUID",
        versionInfo: "Version Info",
        localVersion: "Current version",
        latestVersion: "Latest version",
        getLatest: "Get the latest config",
        downloadWait: "Downloading, please wait..",
        successInfo:
            "Successfully downloaded the latest OC config to ~/Desktop/Tongfang_EFI. Please copy `BOOT` and `OC` folder to ESP.",
        successInstructionUSB:
            "For users booting from USB flash: Copy `BOOT` and `OC` folder to `EFI/BOOT` and `EFI/OC` in your ESP. If `EFI/OC` has already existed in your ESP, please delete it before copy.",
        successInstructionHD:
            "For users booting from hard disk: Rename `BOOT/BOOTx64.efi` to `BOOT/OpenCore.efi`, and copy `BOOT` and `OC` folder to `EFI/BOOT` and `EFI/OC` in your ESP, then add a boot entry pointing to `BOOT/OpenCore.efi`. If `EFI/OC` has already existed in your ESP, please delete it before copy.",
        backward: "Having fun hackintoshing!",
        youAreUsing: "You are using ",
        officialLatest: "The latest version is ",
        updateRemind:
            "Please consider update Tongfang Hackintosh Utility from https://starbeat.kirainmoe.com to avoid compatibility problems.",
        requirement4k:
            'Warning: For Tongfang GJ5CN64 / GI5CN54 barebones\' user, you need to set "DVMT Pre-allocated" to 64MB by unlocking BIOS or using a UEFI shell, or you will meet with a kernel panic。',
        downloadSource: "Download From",
        recommend: 'Recommend'
    }
};

export const str = name => {
    const defaultLanguage = "en",
        userLang = navigator.language;
    if (strings[userLang] && strings[userLang][name]) return strings[userLang][name];
    else return strings[defaultLanguage][name];
};

export default str;
